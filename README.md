jQurrency
=========

jQurrency is a jQuery script to convert fields with multiple currencies to a single currency of your choice using a drop-down menu. jQurrency gathers data from the yahoo currency exchange API and uses it to manage currency conversion in real time.

#USAGE
jQurrency converts every field that has a class ```".price"``` from their base currency (```EUR``` in this case) to the desired currency.
```html
<span class=".price EUR">20</span>
```
