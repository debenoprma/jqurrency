(function( $ ){
    $.fn.curry = function( options ) {
        var $document = $(document);
        var currencies = ["USD", "GBP", "EUR"];
        var rates = {};
        var selector = $('#currency-selector');
        var price_fields = $('.price');

        var symbols_default = {
            'USD' : '&#36;',
            'GBP' : '&pound;',
            'EUR' : '&euro;'
        };

        var init = function(){
            var currencyPairs = generateCurrencyPairs();
            getCurrencyData(currencyPairs);
        };

        // Pilla les dades de la API de yahoo
        var getCurrencyData = function(currencyPairs){
            var string_params = currencyPairs.join(',');
            var json_data = $.ajax({
                url: 'http://query.yahooapis.com/v1/public/yql',
                dataType: 'jsonp',
                data: {
                    q :         'select * from yahoo.finance.xchange where pair="'+string_params+'"',
                    format :    'json',
                    env :       'store://datatables.org/alltableswithkeys'
                }
            });

            json_data
                .done(function( data ){
                    var items = data.query.results.rate;
                    var item;
                    for (var i = 0, l = items.length; i < l; i++){
                        item = items[i];
                        rates[item.id] = item.Rate;
                    }
                    generateDDM();
                    updatePricesOnLoad();
                })
                .fail(function( err ){
                    console.log( err );
                });
        };

        // Genera totes les parelles de currency que necessitem per demanar a la API de yahoo
        var generateCurrencyPairs = function(){
            var len = currencies.length;
            var pairs = [];
            for(var i = 0; i < len; i++) {
                for(var j = 0; j < len; j++) {
                    if (i !== j){
                        pairs.push(currencies[i]+currencies[j]);
                    }
                }
            }
            return pairs;
        };

        // Reescriu els preus a tots els camps price
        var writePrices = function(currency){
            price_fields.each(function(){
                var $this = $(this);
                for(var i = 0, l = currencies.length; i < l; i++){
                    if($this.hasClass(currencies[i])){
                        var converted = $(this).data('base-figure');
                        if(currencies[i] !== currency){
                            converted = converted * rates[currencies[i]+currency];
                        }
                        symbol = symbols_default[currency] || currency;
                        $(this).html( '<span class="symbol">' + symbols_default[currency] + '</span>' + converted.toFixed(2));
                    }
                }
            });
        };

        // Quan es carrega la pagina fa un setup dels preus a partir de una cookie
        var updatePricesOnLoad = function(){
            var currency = $.cookie('site_currency_choice');
            currency = currency || "USD";
            selector.children("select").val(currency);
            price_fields.each(function() {
                var money = $(this).text();
                money = Number(money.replace(/[^0-9\.]+/g,""));
                $(this).data('base-figure', money);
            });
            writePrices(currency);
        };

        // Genera el dropdown menu
        var generateDDM = function(){
            var output = '';
            selector.each(function(){
                output += '<select>';
                for(var i = 0, l = currencies.length; i < l; i++){
                    output += '<option value=' + currencies[i] + '>' + currencies[i] + '</option>';
                }
                output += '</select>';

                $(output).appendTo(this);
            });
        };

        $document.on('change', selector, function(){
            var $option = selector.find(':selected');
            writePrices($option.val());
            $.removeCookie('site_currency_choice', { path: '/' });
            $.cookie('site_currency_choice', $option.val(), { path: '/' });
        });

        init();
    }
})( jQuery );

(function ($) {
    $(function(){
        $('#currency-selector').curry();
    });
})(jQuery);
